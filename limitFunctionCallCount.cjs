// function limitFunctionCallCount(cb, n) {
//     // Should return a function that invokes `cb`.
//     // The returned function should only allow `cb` to be invoked `n` times.
//     // Returning null is acceptable if cb can't be returned
// }


let limitFunctionCallCount = (cb, n) => {
    if(typeof n != 'number' || typeof cb != 'function' || n < 1){
        throw new Error("Invalid parameters.Please provide a callback function and a positive number");
    }
    let returnFunction = (...args) => {
        while(n-- > 0){
            return cb(...args)
        }
        if(n <= 0){
            return null
        }
    }
    return returnFunction
}

module.exports = limitFunctionCallCount;