let limitFunctionCallCount = require('../limitFunctionCallCount.cjs')

let cb = (num) => {
    return num**2
}

let sample = limitFunctionCallCount(cb, 'a')

if(typeof sample === 'function'){
    for(let index = 0; index < 10; index++){
        console.log(sample(10))
    }
}
else{
    console.log(sample)
}



