let cacheFunction = require('../cacheFunction.cjs')

let cb = (num) => {
    return num**2
}

let sample = cacheFunction(cb)
console.log(sample(10))
console.log(sample(6))
console.log(sample(7))
console.log(sample(8))
console.log(sample(7))



