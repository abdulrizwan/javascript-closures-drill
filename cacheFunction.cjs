// function cacheFunction(cb) {
//     // Should return a function that invokes `cb`.
//     // A cache (object) should be kept in closure scope.
//     // The cache should keep track of all arguments have been used to invoke this function.
//     // If the returned function is invoked with arguments that it has already seen
//     // then it should return the cached result and not invoke `cb` again.
//     // `cb` should only ever be invoked once for a given set of arguments.
// }


let cacheFunction = (cb) => {
    if (typeof cb !== "function") {
        throw new Error("Invalid parameter.Please provide a callback function.");
    }
    let usedData = {}

    let returnFunction = (...args) => {
        if (args.length === 0) {
            return cb()
        }

        const key = JSON.stringify(args);
        
        if (key in usedData) {
            return usedData[key]
        }

        let result = cb(...args)
        usedData[key] = result
        return result
    }

    return returnFunction
}

module.exports = cacheFunction